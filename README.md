## Source1 2013 SDK

This repository contains some significant changes compared to valve's original repository,  
the aim is to make it more GIT friendly and therefore easier to use for mods.

Please make sure to read the [LICENSE](LICENSE) and [3rd-Party legal notices](thirdpartylegalnotices.txt) before proceeding.

Additionally to the split into branches there are also **separate repositories with custom changes to improve the overall experience**.

The new structure is as follows:

* `mp/game/mod_hl2mp` => [mp-hl2mp](https://gitlab.com/source1/2013/sdk/tree/mp-hl2mp) branch / [mp-hl2mp](https://gitlab.com/source1/2013/mp-hl2mp) repository
* `mp/src` => [mp-src](https://gitlab.com/source1/2013/sdk/tree/mp-src) branch / [mp-src](https://gitlab.com/source1/2013/mp-src) repository

* `sp/game/ep2/scenes` => [sp-scenes](https://gitlab.com/source1/2013/sdk/tree/sp-scenes) branch / [sp-scenes](https://gitlab.com/source1/2013/sp-scenes) repository
* `sp/game/episodic/scenes` => [sp-scenes](https://gitlab.com/source1/2013/sdk/tree/sp-scenes) branch / [sp-scenes](https://gitlab.com/source1/2013/sp-scenes) repository
* `sp/game/hl2/scenes` => [sp-scenes](https://gitlab.com/source1/2013/sdk/tree/sp-scenes) branch / [sp-scenes](https://gitlab.com/source1/2013/sp-scenes) repository
* `sp/game/lostcoast/scenes` => [sp-scenes](https://gitlab.com/source1/2013/sdk/tree/sp-scenes) branch / [sp-scenes](https://gitlab.com/source1/2013/sp-scenes) repository

* `sp/game/mod_episodic` => [sp-episodic](https://gitlab.com/source1/2013/sdk/tree/sp-episodic) branch / [sp-episodic](https://gitlab.com/source1/2013/sp-episodic) repository
* `sp/game/mod_hl2` => [sp-hl2](https://gitlab.com/source1/2013/sdk/tree/sp-hl2) branch / [sp-hl2](https://gitlab.com/source1/2013/sp-hl2) repository
* `sp/src` => [sp-src](https://gitlab.com/source1/2013/sdk/tree/sp-src) branch / [sp-src](https://gitlab.com/source1/2013/sp-src) repository
